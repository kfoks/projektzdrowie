var diets = [
    {
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 19,
        "bmi_to": 24,
        "age_from": 1,
        "age_to": 24,
        "description": "Jedz owoce i warzywa, a niewiele słodyczy czy tłuszczów. Jak chcesz to możesz robić ćwiczenia wymienione poniżej dla zwiększenia masy mięśniowej.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },
    {
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 20,
        "bmi_to": 25,
        "age_from": 25,
        "age_to": 34,
        "description": "Jedz owoce i warzywa, a niewiele słodyczy czy tłuszczów. Jak chcesz to możesz robić ćwiczenia wymienione poniżej dla zwiększenia masy mięśniowej.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },
    {
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 21,
        "bmi_to": 26,
        "age_from": 35,
        "age_to": 44,
        "description": "Jedz owoce i warzywa, a niewiele słodyczy czy tłuszczów. Jak chcesz to możesz robić ćwiczenia wymienione poniżej dla zwiększenia masy mięśniowej.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },
    {
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 22,
        "bmi_to": 27,
        "age_from": 45,
        "age_to": 54,
        "description": "Jedz owoce i warzywa, a niewiele słodyczy czy tłuszczów. Jak chcesz to możesz robić ćwiczenia wymienione poniżej dla zwiększenia masy mięśniowej.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },
    {
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 23,
        "bmi_to": 28,
        "age_from": 55,
        "age_to": 64,
        "description": "Jedz owoce i warzywa, a niewiele słodyczy czy tłuszczów. Jak chcesz to możesz robić ćwiczenia wymienione poniżej dla zwiększenia masy mięśniowej.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },
    {
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 24,
        "bmi_to": 29,
        "age_from": 65,
        "age_to": 144,
        "description": "Jedz owoce i warzywa, a niewiele słodyczy czy tłuszczów. Jak chcesz to możesz robić ćwiczenia wymienione poniżej dla zwiększenia masy mięśniowej.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },
    {
        "id": 1,
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 25,
        "bmi_to": 100,
        "age_from": 1,
        "age_to": 24,
        "description": "Staraj się jeść mniej tłustych rzeczy m.in. roślin oleistych, czy jedzenia smażonego w oleju lub na oleju.",
        "youtube_links": [
            "https://www.youtube.com/embed/ttGvFyeY_Oc",
            "https://www.youtube.com/embed/62W4Rb56bH0"
        ]
    },
    {
        "id": 2,
        "goal_ids": [
            "2",
            "3"
        ],
        "bmi_from": 1,
        "bmi_to": 18,
        "age_from": 1,
        "age_to": 24,
        "description": "Spożywaj więcej tłuszczów dodając do swojej diety produkty t.j. orzechy, siemię lniane, nasiona dyni, awokado, oliwki, lecz nie przesadź z ich ilością. Jedz więcej jedzenia zawierającego białko m.in. nabiał, fasola.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },

    {
        "id": 3,
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 26,
        "bmi_to": 100,
        "age_from": 25,
        "age_to": 34,
        "description": "Staraj się jeść mniej tłustych rzeczy m.in. roślin oleistych, czy jedzenia smażonego w oleju lub na oleju.",
        "youtube_links": [
            "https://www.youtube.com/embed/ttGvFyeY_Oc",
            "https://www.youtube.com/embed/62W4Rb56bH0"
        ]
    },
    {
        "id": 4,
        "goal_ids": [
            "2",
            "3"
        ],
        "bmi_from": 1,
        "bmi_to": 19,
        "age_from": 25,
        "age_to": 34,
        "description": "Spożywaj więcej tłuszczów dodając do swojej diety produkty t.j. orzechy, siemię lniane, nasiona dyni, awokado, oliwki, lecz nie przesadź z ich ilością. Jedz więcej jedzenia zawierającego białko m.in. nabiał, fasola.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },

    {
        "id": 5,
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 27,
        "bmi_to": 100,
        "age_from": 35,
        "age_to": 44,
        "description": "Staraj się jeść mniej tłustych rzeczy m.in. roślin oleistych, czy jedzenia smażonego w oleju lub na oleju.",
        "youtube_links": [
            "https://www.youtube.com/embed/ttGvFyeY_Oc",
            "https://www.youtube.com/embed/62W4Rb56bH0"
        ]
    },
    {
        "id": 6,
        "goal_ids": [
            "2",
            "3"
        ],
        "bmi_from": 1,
        "bmi_to": 20,
        "age_from": 35,
        "age_to": 44,
        "description": "Spożywaj więcej tłuszczów dodając do swojej diety produkty t.j. orzechy, siemię lniane, nasiona dyni, awokado, oliwki, lecz nie przesadź z ich ilością. Jedz więcej jedzenia zawierającego białko m.in. nabiał, fasola.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },

    {
        "id": 7,
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 28,
        "bmi_to": 100,
        "age_from": 45,
        "age_to": 54,
        "description": "Staraj się jeść mniej tłustych rzeczy m.in. roślin oleistych, czy jedzenia smażonego w oleju lub na oleju.",
        "youtube_links": [
            "https://www.youtube.com/embed/ttGvFyeY_Oc",
            "https://www.youtube.com/embed/62W4Rb56bH0"
        ]
    },
    {
        "id": 8,
        "goal_ids": [
            "2",
            "3"
        ],
        "bmi_from": 1,
        "bmi_to": 21,
        "age_from": 45,
        "age_to": 54,
        "description": "Spożywaj więcej tłuszczów dodając do swojej diety produkty t.j. orzechy, siemię lniane, nasiona dyni, awokado, oliwki, lecz nie przesadź z ich ilością. Jedz więcej jedzenia zawierającego białko m.in. nabiał, fasola.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },

    {
        "id": 9,
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 29,
        "bmi_to": 100,
        "age_from": 55,
        "age_to": 64,
        "description": "Staraj się jeść mniej tłustych rzeczy m.in. roślin oleistych, czy jedzenia smażonego w oleju lub na oleju.",
        "youtube_links": [
            "https://www.youtube.com/embed/ttGvFyeY_Oc",
            "https://www.youtube.com/embed/62W4Rb56bH0"
        ]
    },
    {
        "id": 10,
        "goal_ids": [
            "2",
            "3"
        ],
        "bmi_from": 1,
        "bmi_to": 22,
        "age_from": 55,
        "age_to": 64,
        "description": "Spożywaj więcej tłuszczów dodając do swojej diety produkty t.j. orzechy, siemię lniane, nasiona dyni, awokado, oliwki, lecz nie przesadź z ich ilością. Jedz więcej jedzenia zawierającego białko m.in. nabiał, fasola.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    },

    {
        "id": 11,
        "goal_ids": [
            "1",
            "2",
            "3"
        ],
        "bmi_from": 30,
        "bmi_to": 100,
        "age_from": 65,
        "age_to": 144,
        "description": "Staraj się jeść mniej tłustych rzeczy m.in. roślin oleistych, czy jedzenia smażonego w oleju lub na oleju.",
        "youtube_links": [
            "https://www.youtube.com/embed/ttGvFyeY_Oc",
            "https://www.youtube.com/embed/62W4Rb56bH0"
        ]
    },
    {
        "id": 12,
        "goal_ids": [
            "2",
            "3"
        ],
        "bmi_from": 1,
        "bmi_to": 23,
        "age_from": 65,
        "age_to": 144,
        "description": "Spożywaj więcej tłuszczów dodając do swojej diety produkty t.j. orzechy, siemię lniane, nasiona dyni, awokado, oliwki, lecz nie przesadź z ich ilością. Jedz więcej jedzenia zawierającego białko m.in. nabiał, fasola.",
        "youtube_links": [
            "https://www.youtube.com/embed/fJiakypClBQ",
            "https://www.youtube.com/embed/TLK2iS3YRqg",
            "https://www.youtube.com/embed/NsIUzjd2kDU",
            "https://www.youtube.com/embed/H_hz7HNqqrQ",
            "https://www.youtube.com/embed/KyyV5Xte6vY",
            "https://www.youtube.com/embed/kwyvG7Nb7Fk",
            "https://www.youtube.com/embed/gCG11TsrY2w",
            "https://www.youtube.com/embed/G98GDoDCFB8"
        ]
    }
]